
# `nin.sh`
`nin.sh` is a script designed to make setting an interface's addressing parameters interactive and simple to do.

## Prerequisites

* `bash`
* Linux platform
* `nmcli`

## Installing
Download nin.sh and give it execution permissions by running `chmod +x nin.sh`
###### or
Copy and paste this into your own text editor and save it with a .sh extension, then give it execution permissions.

## Syntax

	./nin.sh [dynamic|static|usage]


## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Bash](https://www.gnu.org/software/bash/)
* [Atom](https://atom.io/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to test as appropriate.

## Authors

* **Brandon Nolet** - [brandon](https://bnolet.me/brandon)
* **Diabolic Preacher** - [@dpreacher](https://mastodon.rocks/@dpreacher)

## License

This project is licensed under the GPLv3 copyleft license. That means that this code and any derivative works are perpetually free - see the [LICENSE.md](LICENSE.md) file for details

## Why Should I Use This?

With basic static/DHCP IPv4 addressing, it's much faster than actually typing out the nmcli commands, especially when you have multiple interfaces to address.
