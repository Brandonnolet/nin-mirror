#!/bin/bash

# Version 0.6
# Author: Brandon Nolet
# Contact: nolet.brandon@gmail.com

# Reading this script:
# Global variables use ALL CAPS
# Non-global variables use UpperCamelCase
# Might add more to this later

function usage() {
	echo "
	Description: This is a basic script that makes use
	of the nmcli command, setting either a static or
	dynamic IPv4 address. The script will take one or
	no arguments. Should no arguments be passed to the
	script, then the user will be prompted for the mode
	to set the interface to. Otherwise the syntax is as follows

	Syntax:
	./nin.sh [dynamic|static|usage]
	"
}

#Global Vars
NMCLI_BIN=$(which nmcli)
TIMESTAMP=$(date +%H:%M:%S)
LOGFILE=nin.log
TEELOG="tee --append ${LOGFILE}"

# `setstatic` works as a state machine, running through
# each case statement in order of operation. This is Done
# by the use of the setstatic function couples with the first
# argument corresponding to the case statement desiring to
# be returned to

function setstatic {

	#Getting Static IP info
case $1 in #case to enable returnability

	1)
		conshow
		setstatic 2
		;;
	2)
		echo
		read -p "Enter the desired IPv4 address: " IPAdd
		echo
		validip 2 3 ${IPAdd}
		;;
	3)
		read -p "Enter the IPv4 prefix length: " PrefLen
		if [[ ${PrefLen} -gt 0 ]] && [[ ${PrefLen} -lt 32 ]]; then #IPs only have 32 bits
			setstatic 4
		else
			echo "Not a valid prefix length."
			echo
			setstatic 3
		fi
		echo
		;;
	4)
		echo
		read -p "Enter the default router/gateway IP: " GatewayIP
		echo
		validip 4 5 ${GatewayIP}
		;;
	5)
		read -p "Enter the DNS server address(es): " DNSAdd
		echo
		validip 5 6 "${DNSAdd}"
		;;

	6)
		#Setting Static IP info
		echo "Configuring..."
		${NMCLI_BIN} connection modify "${IntID}" ipv4.addresses ${IPAdd}/${PrefLen}
		${NMCLI_BIN} connection modify "${IntID}" ipv4.gateway ${GatewayIP}
		${NMCLI_BIN} connection modify "${IntID}" ipv4.dns "${DNSAdd}"  #Surrounded in quotes for multiple DNS servers
		${NMCLI_BIN} connection modify "${IntID}" ipv4.method manual
	  echo "${TIMESTAMP}: IPv4 address of ${IntID} set to ${IPAdd}/${PrefLen}." >> ${LOGFILE}
		echo "${TIMESTAMP}: ${IntID} default gateway set to ${GatewayIP}." >> ${LOGFILE}
		echo "${TIMESTAMP}: ${IntID}'s DNS servers are now ${DNSAdd}." >> ${LOGFILE}
		echo "${TIMESTAMP}: ${IntID}'s resulting addressing method is \"Static\"" >> ${LOGFILE}

		reloadint
		;;
esac
}

function setdynamic() {
	conshow

	echo
	echo "Configuring..."
	#This is all that's really needed to set the interface to
	#DHCP addressing
	${NMCLI_BIN} connection modify "${IntID}" ipv4.method auto

	reloadint
}

function reloadint() {

	#Cycles the state of the selected interface
	#to apply the changes
	echo "The network interface will be momentarily disconnected."
	sleep 1 #cosmetic
	read -s -p "Please pause your downloads and whatnot, then press enter when ready."; echo


	echo "Applying changes..."
  ${NMCLI_BIN} connection down "${IntID}" | sed "s/^/${TIMESTAMP}: /" >> ${LOGFILE}
	sleep 1 #for effect
  ${NMCLI_BIN} connection up "${IntID}" | sed "s/^/${TIMESTAMP}: /" >> ${LOGFILE}
  echo "Done!"
}

function conshow() {

	echo
	${NMCLI_BIN} connection show --active | ${TEELOG}

	echo
	read -p "Type the name of the interface you wish to configure: " IntID
	echo "User input: IntID=${IntID}" >> ${LOGFILE}

	if [[ "${IntID}" != '' ]]; then #empty strings make grep have a fit
		${NMCLI_BIN} -t connection show | cut -d : -f 1 | grep "${IntID}" &> /dev/null
		if [[ $? != 0 ]]; then #checking for valid interface name being input
			echo
			echo "Sorry, that's not the name of a valid interface. Try again." | ${TEELOG}
			echo
			sleep 1

			conshow #would love to have a non-inception like way to do this.
		else
			#incomplete entry of interface results in completion by grep ;) Interesting I suppose
			IntID=$(${NMCLI_BIN} -t con show | cut -d : -f 1 | grep "${IntID}" | head -1)
			return 0 #If the input matches the name of an interface, move on
		fi
	else #empty string is empty, prompt again
		echo
		echo "Sorry, that's not the name of a valid interface, try again"
		echo
		sleep 1
		conshow
	fi
	echo

}

# `validip` is meant to check for proper IP address formatting
# `validip` accepts 3 arguments. The first is the state (an integer)
# to return to on incorrect format detection. The second is the state
# to move to on success

function validip { #made specifically for the setstatic state machine
	declare -i n=0 #Need some math up in here!
	for i in $3; do #with DNS, multiple IPs need to be parsed for format
	# VVV it's ugly...but it works
	echo $i | egrep -o "^((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-fA-F]|[a-fA-F][a-fA-F0-9\-]*[a-fA-F0-9])\.)*([A-Fa-f]|[A-Fa-f][A-Fa-f0-9\-]*[A-Fa-f0-9])$|^(?:(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){6})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:::(?:(?:(?:[0-9a-fA-F]{1,4})):){5})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){4})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,1}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){3})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,2}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:(?:[0-9a-fA-F]{1,4})):){2})(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,3}(?:(?:[0-9a-fA-F]{1,4})))?::(?:(?:[0-9a-fA-F]{1,4})):)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,4}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,5}(?:(?:[0-9a-fA-F]{1,4})))?::)(?:(?:[0-9a-fA-F]{1,4})))|(?:(?:(?:(?:(?:(?:[0-9a-fA-F]{1,4})):){0,6}(?:(?:[0-9a-fA-F]{1,4})))?::)))))$" > /dev/null
	n=n+$? #if ANY of the passed IPs are invalid, this increments
	done
	if [ ${n} == 0 ]; then
		setstatic $2 #if all is good, move on to next state
	else
		echo
		echo $3 >> ${LOGFILE}
		echo "Sorry, that's not a valid IP. Try again." | ${TEELOG}
		echo
		setstatic $1 #return to previous state
	fi

}

function getchoice() {

	if [[ $1 == '' ]]; then
		read -p "Do you want to configure static or DHCP addressing? (static/dhcp) " CHOICE
		CHOICE=`echo ${CHOICE}|tr '[:upper:]' '[:lower:]'`
	else
		CHOICE=`echo $1|tr '[:upper:]' '[:lower:]'`
	fi
	case ${CHOICE} in
		dhcp)
			setdynamic
			;;
		static)
			setstatic 1
			;;
		usage)
			usage
			;;
		*)
			echo
			echo "Sorry, accepted values are 'dhcp' or 'static' without quotes."
			# echo "Try ./nin.sh usage for more information." Will put back when more options are available.
			echo
			sleep 1
			getchoice
			;;
	esac
}

echo "Start of script run: ${TIMESTAMP}" >> $LOGFILE
getchoice $1
echo "End of script run: ${TIMESTAMP}" >> $LOGFILE

exit 0


# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
